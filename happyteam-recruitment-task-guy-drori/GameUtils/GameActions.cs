﻿using happyteam_recruitment_task_guy_drori.Models;
using happyteam_recruitment_task_guy_drori.Models.Enums;
using happyteam_recruitment_task_guy_drori.Utils;

namespace happyteam_recruitment_task_guy_drori.GameUtils
{
    public class GameActions
    {
        public static TurnResult PlayTurnByUser(Game game)
        {
            try
            {
                string? position = null;
                BoardPosition? boardPosition = null;
                var parsingSuccess = false;
                while (position == null || !ValidationUtils.IsPositionValid(position) || !parsingSuccess)
                {
                    Console.WriteLine("Please enter a position, starting with the letter for the row and number for the column (e.g. A1) and then press Enter to attempt a hit:");
                    position = Console.ReadLine();
                    if (position != null)
                    {
                        position = position.ToUpper();
                        if (!ValidationUtils.IsPositionValid(position))
                        {
                            Console.WriteLine("Invalid position, please try again!");
                        }
                        else
                        {
                            parsingSuccess = TryParsePosition(position, out boardPosition);
                            if (!parsingSuccess)
                            {
                                Console.WriteLine("Invalid position, please try again!");
                            }
                        }
                    }
                }
                if (boardPosition != null)
                {
                    var hitShip = game.Hit(boardPosition);
                    if (hitShip == null)
                    {
                        Console.WriteLine("Miss!");
                        return TurnResult.Miss;
                    }
                    else
                    {
                        if (hitShip.Sunk)
                        {
                            Console.WriteLine($"Ship sunk!, Type: {hitShip.Type.ToReadableString()}.{(game.WinningPlayer == null ? " You have another turn!" : "")}");
                        }
                        else
                        {
                            Console.WriteLine($"Hit!{(game.WinningPlayer == null ? " You have another turn!" : "")}");
                        }
                        return TurnResult.Hit;
                    }
                }
                else
                {
                    throw new Exception("Board position error");
                }
            } catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                return TurnResult.Miss;
            }
        }

        public static TurnResult PlayTurnByComputer(Game game)
        {
            var boardPosition = PositionGenerator.GenerateRandomPosition();
            var hitShip = game.Hit(boardPosition);
            if (hitShip == null)
            {
                Console.WriteLine("Computer missed!");
                return TurnResult.Miss;
            }
            else
            {
                if (hitShip.Sunk)
                {
                    Console.WriteLine($"Ship sunk!, Type: {hitShip.Type.ToReadableString()}.{(game.WinningPlayer == null ? " The computer has another turn!" : "")}");
                }
                else
                {
                    Console.WriteLine($"Computer hit!{(game.WinningPlayer == null ? " It has another turn!" : "")}");
                }
                return TurnResult.Hit;
            }
        }

        private static bool TryParsePosition(string position, out BoardPosition? boardPosition)
        {
            if (!ValidationUtils.IsPositionValid(position))
            {
                throw new ArgumentException("Invalid position string!");
            }
            var regexMatch = RegularExpressions.BOARD_POSITION.Match(position);
            var columnString = regexMatch.Groups["column"].Value;
            int column = -1;
            var columnStringParsingSuccess = int.TryParse(columnString, out column);
            if (!columnStringParsingSuccess)
            {
                boardPosition = null;
                return false;
            } else
            {
                boardPosition = new BoardPosition(regexMatch.Groups["row"].Value, column);
                return true;
            }
        }
    }
}
