﻿using happyteam_recruitment_task_guy_drori.GameUtils;
using happyteam_recruitment_task_guy_drori.Models;

var game = new Game();
Console.WriteLine("Welcome to Battleships!");
Console.WriteLine($"Your board is:\n{game.User.Board}");
while (game.WinningPlayer == null)
{
    if (game.CurrentPlayer == game.User)
    {
        GameActions.PlayTurnByUser(game);
    }
    else
    {
        GameActions.PlayTurnByComputer(game);
    }
}
if (game.WinningPlayer == game.User)
{
    Console.WriteLine("You win!");
}
else
{
    Console.WriteLine("Computer wins!");
}
