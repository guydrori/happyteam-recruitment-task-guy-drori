﻿using happyteam_recruitment_task_guy_drori.Models;
using happyteam_recruitment_task_guy_drori.Models.Enums;

namespace happyteam_recruitment_task_guy_drori.Utils
{
    public class ShipUtils
    {
        private static readonly IDictionary<string, int> _rowToDigitDictionary = new Dictionary<string, int>()
        {
            {"A", 1},
            {"B", 2},
            {"C", 3},
            {"D", 4},
            {"E", 5},
            {"F", 6},
            {"G", 7},
            {"H", 8},
            {"I", 9},
            {"J", 10}
        };

        public static IDictionary<string, int> RowToDigitDictionary { get { return _rowToDigitDictionary; } }

        private static readonly IDictionary<int, string> _digitToRowDictionary = new Dictionary<int, string>()
        {
            {1, "A"},
            {2, "B"},
            {3, "C"},
            {4, "D"},
            {5, "E"},
            {6, "F"},
            {7, "G"},
            {8, "H"},
            {9, "I"},
            {10, "J"}
        };

        public static IDictionary<int, string> DigitToRowDictionary { get { return _digitToRowDictionary; } }

        public static ISet<BoardPosition> GeneratePositions(ShipType shipType, BoardPosition startingPosition, ShipDirection direction)
        {
            var positions = new HashSet<BoardPosition>();
            if (direction == ShipDirection.HORIZONTAL)
            {
                for (int i = 1; i <= shipType.GetLength(); i++)
                {
                    int newColumn = startingPosition.Column + i;
                    if (newColumn > 10)
                    {
                        throw new ArgumentOutOfRangeException("Ship out of bounds");
                    }
                    positions.Add(new BoardPosition(startingPosition.Row, newColumn));
                }
            }
            else
            {
                int rowDigit = RowToDigitDictionary[startingPosition.Row];
                for (int i = 1; i <= shipType.GetLength(); i++)
                {
                    if ((rowDigit + i) > 10)
                    {
                        throw new ArgumentOutOfRangeException("Ship out of bounds");
                    }
                    var newRow = DigitToRowDictionary[rowDigit + i];
                    positions.Add(new BoardPosition(newRow, startingPosition.Column));
                }
            }
            return positions;
        }
    }
}
