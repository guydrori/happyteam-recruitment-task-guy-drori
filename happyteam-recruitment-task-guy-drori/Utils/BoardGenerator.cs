﻿using happyteam_recruitment_task_guy_drori.Models;
using happyteam_recruitment_task_guy_drori.Models.Enums;

namespace happyteam_recruitment_task_guy_drori.Utils
{
    public class BoardGenerator
    {
        private static BoardPosition GetRandomStartingBoardPosition(ShipType shipType, ShipDirection direction)
        {
            var randomGenerator = new Random();
            string? randomRow = null;
            int? randomColumn = null;
            if (direction == ShipDirection.HORIZONTAL)
            {
                randomRow = ShipUtils.DigitToRowDictionary[randomGenerator.Next(1, 10)];
                randomColumn = randomGenerator.Next(1, 10 - shipType.GetLength());
            }
            else
            {
                randomRow = ShipUtils.DigitToRowDictionary[randomGenerator.Next(1, 10 - shipType.GetLength())];
                randomColumn = randomGenerator.Next(1, 10);
            } 
            return new BoardPosition(randomRow, randomColumn.Value);
        }

        private static ShipDirection GetRandomShipDirection()
        {
            var randomGenerator = new Random();
            var randomSelection = randomGenerator.Next(0, 1);
            if (randomSelection == 1)
            {
                return ShipDirection.HORIZONTAL;
            }
            else
            {
                return ShipDirection.VERTICAL;
            }
        }

        private static Ship GenerateShip(ShipType type)
        {
            var direction = GetRandomShipDirection();
            var startingPosition = GetRandomStartingBoardPosition(type,direction);
            while (IsStartingPositionInvalid(type, startingPosition, direction))
            {
                startingPosition = GetRandomStartingBoardPosition(type,direction);
                direction = GetRandomShipDirection();
            }
            return new Ship(type, startingPosition, direction);
        }

        private static bool IsStartingPositionInvalid(ShipType type, BoardPosition startingPosition, ShipDirection direction)
        {
            if (direction == ShipDirection.HORIZONTAL)
            {
                var endingColumn = startingPosition.Column + (type.GetLength() - 1);
                return endingColumn > 10;
            }
            else if (direction == ShipDirection.VERTICAL)
            {
                var endingRowIndex = ShipUtils.RowToDigitDictionary[startingPosition.Row] + (type.GetLength() - 1);
                return endingRowIndex > 10;
            }
            return true;
        }

        public static Board GenerateRandomBoard()
        {
            var ships = new HashSet<Ship>();
            ships.Add(GenerateShip(ShipType.Carrier));
            ships.Add(GenerateShip(ShipType.Battleship));
            ships.Add(GenerateShip(ShipType.Destroyer));
            ships.Add(GenerateShip(ShipType.Submarine));
            ships.Add(GenerateShip(ShipType.PatrolBoat));
            try
            {
                return new Board(ships);
            } catch (Exception)
            {
                return GenerateRandomBoard();
            }
        }
    }
}
