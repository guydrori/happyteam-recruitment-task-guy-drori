﻿using happyteam_recruitment_task_guy_drori.Models;
using happyteam_recruitment_task_guy_drori.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace happyteam_recruitment_task_guy_drori.Utils
{
    public static class ValidationUtils
    {
        public static bool AreShipPositionsValid(ISet<BoardPosition> boardPositions)
        {
            ISet<string> rows = new SortedSet<string>();
            ISet<int> columns = new SortedSet<int>();
            foreach (BoardPosition boardPosition in boardPositions)
            {
                rows.Add(boardPosition.Row);
                columns.Add(boardPosition.Column);
            }
            IEnumerator<int>? rowOrColumnEnumerator = null;
            if (rows.Count == 1)
            {
                rowOrColumnEnumerator = columns.GetEnumerator();
            }
            else
            {
                if (columns.Count > 1)
                {
                    return false;
                }
                rowOrColumnEnumerator = rows.Select(row => ShipUtils.RowToDigitDictionary[row]).GetEnumerator();
            }
            int? value1 = null;
            if (rowOrColumnEnumerator.MoveNext())
            {
                value1 = rowOrColumnEnumerator.Current;
                while (rowOrColumnEnumerator.MoveNext())
                {
                    int value2 = rowOrColumnEnumerator.Current;
                    if (Math.Abs(value2 - value1.Value) > 1)
                    {
                        return false;
                    }
                    value1 = value2;
                }    
            }
            return true;
        }

        public static bool IsPositionValid(string position)
        {
            if (string.IsNullOrWhiteSpace(position) || !RegularExpressions.BOARD_POSITION.IsMatch(position))
            {
                return false;
            }
            var regexMatch = RegularExpressions.BOARD_POSITION.Match(position);
            var columnString = regexMatch.Groups["column"].Value;
            int column = -1;
            var columnStringParsingSuccess = int.TryParse(columnString, out column);
            if (!columnStringParsingSuccess)
            {
                return false;
            }
            return column > 0 && column <= 10;
        }

        public static bool AreShipsNotOverlapping(ISet<Ship> ships)
        {
            ISet<BoardPosition> positionSet = new HashSet<BoardPosition>();
            foreach (Ship ship in ships)
            {
                foreach (BoardPosition boardPosition in ship.CoveredPositions)
                {
                    bool added = positionSet.Add(boardPosition);
                    if (!added)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
