﻿using happyteam_recruitment_task_guy_drori.Models;

namespace happyteam_recruitment_task_guy_drori.Utils
{
    public class PositionGenerator
    {
        public static BoardPosition GenerateRandomPosition()
        {
            Random randomGenerator = new Random();
            var row = ShipUtils.DigitToRowDictionary[randomGenerator.Next(1, 10)];
            var column = randomGenerator.Next(1, 10);
            return new BoardPosition(row, column);
        }
    }
}
