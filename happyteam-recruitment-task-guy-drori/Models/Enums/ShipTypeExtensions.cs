﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace happyteam_recruitment_task_guy_drori.Models.Enums
{
    public static class ShipTypeExtensions
    {
        public static int GetLength(this ShipType shipType) 
        {
            switch (shipType) 
            {
                case ShipType.Carrier: return 5;
                case ShipType.Battleship: return 4;
                case ShipType.Destroyer: return 3;
                case ShipType.Submarine: return 3;
                case ShipType.PatrolBoat: return 2;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        public static string ToReadableString(this ShipType shipType)
        {
            if (shipType == ShipType.PatrolBoat)
            {
                return "Patrol Boat";
            }
            else
            {
                return shipType.ToString();
            }
        }
    }
}
