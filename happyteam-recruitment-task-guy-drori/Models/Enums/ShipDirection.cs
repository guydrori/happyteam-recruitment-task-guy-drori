﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace happyteam_recruitment_task_guy_drori.Models.Enums
{
    public enum ShipDirection
    {
        VERTICAL,
        HORIZONTAL
    }
}
