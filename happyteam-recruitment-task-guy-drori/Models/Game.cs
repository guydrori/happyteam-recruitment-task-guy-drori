﻿
namespace happyteam_recruitment_task_guy_drori.Models
{
    public class Game
    {
        private Player _user = new Player();

        public Player User { get { return _user; } }

        private Player _computer = new Player();

        public Player Computer { get { return _computer; } }

        private Player _currentPlayer;

        public Player? CurrentPlayer { get { return _currentPlayer; } }

        private Player? _winningPlayer;

        public Player? WinningPlayer { get { return _winningPlayer; } }

        public Game()
        {
            Random random = new Random();
            var playerSelection = random.Next(1, 2);
            if (playerSelection == 1)
            {
                _currentPlayer = User;
            }
            else
            {
                _currentPlayer = Computer;
            }
        }

        /// <summary>
        /// Attempts to perform a hit at the given position on the opponent's board
        /// </summary>
        /// <param name="positionToHit">The board position to hit</param>
        /// <returns>the hit ship whether the hit was successful, null otherwise</returns>
        public Ship? Hit(BoardPosition positionToHit)
        {
            Ship? hitShip = null;
            if (CurrentPlayer != null)
            {
                Player targetPlayer;
                if (CurrentPlayer == User)
                {
                    targetPlayer = Computer;
                }
                else
                {
                    targetPlayer = User;
                }
                foreach (Ship ship in targetPlayer.Board.Ships)
                {
                    if (ship.TryHit(positionToHit))
                    {
                        CurrentPlayer.IncrementScore();
                        if (CurrentPlayer.Score == targetPlayer.Board.WinningScore && WinningPlayer == null)
                        {
                            _winningPlayer = CurrentPlayer;
                        }
                        hitShip = ship;
                    }
                }
                if (hitShip == null)
                {
                    if (CurrentPlayer == User)
                    {
                        _currentPlayer = Computer;
                    }
                    else
                    {
                        _currentPlayer = User;
                    }
                }
            }
            return hitShip;
        }
    }
}
