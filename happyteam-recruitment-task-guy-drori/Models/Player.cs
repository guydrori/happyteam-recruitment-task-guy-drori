﻿using happyteam_recruitment_task_guy_drori.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace happyteam_recruitment_task_guy_drori.Models
{
    public class Player : IEquatable<Player?>
    {
        private Board _board;

        public Board Board { get { return _board; } }

        private int _score = 0;

        public int Score { get { return _score; } }

        public void IncrementScore()
        {
            _score++;
        }

        public Player()
        {
            _board = BoardGenerator.GenerateRandomBoard();
        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as Player);
        }

        public bool Equals(Player? other)
        {
            return other != null &&
                   EqualityComparer<Board>.Default.Equals(Board, other.Board) &&
                   Score == other.Score;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Board, Score);
        }

        public static bool operator ==(Player? left, Player? right)
        {
            return EqualityComparer<Player>.Default.Equals(left, right);
        }

        public static bool operator !=(Player? left, Player? right)
        {
            return !(left == right);
        }
    }
}
