﻿using happyteam_recruitment_task_guy_drori.Models.Enums;
using happyteam_recruitment_task_guy_drori.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace happyteam_recruitment_task_guy_drori.Models
{
    public class Ship : IEquatable<Ship?>
    {

        private ShipType _type;

        public ShipType Type
        {
            get { return _type; }
        }

        private ISet<BoardPosition> _coveredPositions;

        public ISet<BoardPosition> CoveredPositions { get { return new SortedSet<BoardPosition>(_coveredPositions); } }

        private ISet<BoardPosition> _hitPositions;

        public ISet<BoardPosition> HitPositions { get { return new SortedSet<BoardPosition>(_hitPositions); } }

        private bool _sunk;

        public bool Sunk { get { return _sunk; } }

        public Ship(ShipType type, BoardPosition startingPosition, ShipDirection direction)
        {
            _type = type;
            ISet<BoardPosition>? coveredPositions = null;
            while (coveredPositions == null)
            {
                try
                {
                    coveredPositions = ShipUtils.GeneratePositions(type, startingPosition, direction);
                } catch (Exception)
                {
                    //Do nothing :)
                }
            }
            if (coveredPositions == null || coveredPositions.Count == 0 || !ValidationUtils.AreShipPositionsValid(coveredPositions))
            {
                throw new ArgumentException("Invalid positions");
            }
            _coveredPositions = coveredPositions;
            _hitPositions = new HashSet<BoardPosition>();
        }

        /// <summary>
        /// Attempts to perform a hit at the given position on the board
        /// </summary>
        /// <param name="positionToHit">The board position to hit</param>
        /// <returns>true whether the hit was successful, false otherwise</returns>
        /// <exception cref="ArgumentNullException">Thrown when the board position given is null</exception>
        public bool TryHit(BoardPosition positionToHit)
        {
            if (positionToHit == null)
            {
                throw new ArgumentNullException();
            }
            else if (!CoveredPositions.Contains(positionToHit) || HitPositions.Contains(positionToHit))
            {
                return false;
            }
            else
            {
                _hitPositions.Add(positionToHit);
                if (CoveredPositions.SequenceEqual(HitPositions))
                {
                    _sunk = true;
                }
                return true;
            }
        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as Ship);
                   
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Type, CoveredPositions, HitPositions);
        }

        public bool Equals(Ship? other)
        {
            return other != null &&
                Type == other.Type &&
                EqualityComparer<ISet<BoardPosition>>.Default.Equals(CoveredPositions, other.CoveredPositions) &&
                EqualityComparer<ISet<BoardPosition>>.Default.Equals(HitPositions, other.HitPositions);
        }

        public static bool operator ==(Ship? left, Ship? right)
        {
            return EqualityComparer<Ship>.Default.Equals(left, right);
        }

        public static bool operator !=(Ship? left, Ship? right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            var sortedPositions = new SortedSet<string>(CoveredPositions.Select(boardPosition=> boardPosition.ToString()));
            return $"Ship Type: {Type.ToReadableString()}, Positions: [${string.Join(',', sortedPositions)}]";
        }
    }
}
