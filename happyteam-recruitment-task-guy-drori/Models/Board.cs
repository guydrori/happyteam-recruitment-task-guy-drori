﻿using happyteam_recruitment_task_guy_drori.Models.Enums;
using happyteam_recruitment_task_guy_drori.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace happyteam_recruitment_task_guy_drori.Models
{
    public class Board : IEquatable<Board?>
    {
        private ISet<Ship> _ships;

        public ISet<Ship> Ships
        {
            get
            {
                return _ships;
            }
        }

        private int _winningScore;

        public int WinningScore { get { return _winningScore;  } }

        public Board(ISet<Ship> ships) 
        {
            if (ships == null)
            {
                throw new ArgumentNullException("value");
            }
            if (!ValidationUtils.AreShipsNotOverlapping(ships))
            {
                throw new ArgumentException("Overlapping ships!");
            }
            _ships = ships;
            _winningScore = _ships.Select(ship => ship.Type.GetLength()).Sum();
        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as Board);
        }

        public bool Equals(Board? other)
        {
            return other != null &&
                   EqualityComparer<ISet<Ship>>.Default.Equals(Ships, other.Ships) &&
                   WinningScore == other.WinningScore;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Ships, WinningScore);
        }

        public static bool operator ==(Board? left, Board? right)
        {
            return EqualityComparer<Board>.Default.Equals(left, right);
        }

        public static bool operator !=(Board? left, Board? right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return string.Join("\n", Ships);
        }
    }
}
