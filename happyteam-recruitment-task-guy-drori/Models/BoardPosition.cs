﻿namespace happyteam_recruitment_task_guy_drori.Models
{
    public class BoardPosition : IEquatable<BoardPosition?>, IComparable<BoardPosition>
    {
        private string _row;
        private int _column;

        public string Row {
            get { return _row; }
        }

        public int Column
        {
            get { return _column; }
        }

        public BoardPosition(string row, int column)
        {
            _row = row;
            _column = column;
        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as BoardPosition);
        }

        public bool Equals(BoardPosition? other)
        {
            return other != null &&
                   Row == other.Row &&
                   Column == other.Column;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Row, Column);
        }

        public static bool operator ==(BoardPosition? left, BoardPosition? right)
        {
            return EqualityComparer<BoardPosition>.Default.Equals(left, right);
        }

        public static bool operator !=(BoardPosition? left, BoardPosition? right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return $"{Row}{Column}";
        }

        public int CompareTo(BoardPosition? obj)
        {
            if (obj == null)
            {
                return 1;
            }
            if (ReferenceEquals(this, obj))
            {
                return 0;
            }
            return this.ToString().CompareTo(obj.ToString());
        }
    }
}
